# A wild dataset appears  

A collection of notebooks to do all the things I should do with a new project
  
There are eight main steps to any Data Science project:

1. Define the problem  
*Frame the problem*  

2. Get the data  
*Where is it, what's the best way to connect to it*  

3. Explore the data 
*Dora that data yo*  
*Write the damn data dictionary as you go*  

4. Clean the data  
*Scrub that data, make it easier for the patterns to show*  

5. Model  
*Try some models, race them*  

6. Parameter tuning  
*Pick the best and make it better*  

7. Present the solution  
*Buff it up and show it off*  
*Write the damn documentation*  

8. Maintain the model  
*Plan operationalisation, monitoring and maintenace*  


**1. Define the problem**  

Define the objective in business terms.  
How will your solution be used?  
What are the current solutions/workarounds (if any)?  
How should you frame this problem (supervised/unsupervised, online/offline, etc.)?  
How should performance be measured?  
Is the performance measure aligned with the business objective?  
What would be the minimum performance needed to reach the business objective?  
What are comparable problems? Can you reuse experience or tools?  
Is human expertise available?  
How would you solve the problem manually?  
List the assumptions you (or others) have made so far.  
Verify assumptions if possible.  

**2. Get the data**  

**3. Explore the data** 

see exploration.Rmd

**4. Clean the data**  
see clean.Rmd

**5. Model**   
**6. Paramater tuning**    
see ...  

**7. Present the solution**  
**8. Maintain the model**  
see documentation.Rmd  

